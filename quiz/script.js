//OBJETO

var perguntas = [
    {
        texto: 'O céu não é o limite',
        respostas: [
            { valor: 0, texto: 'Ter preguiça' },
            { valor: 1, texto: 'Quando der certo a gente faz' },
            { valor: 3, texto: 'Vamo pra cima familia' },
            { valor: 2, texto: 'Se der for viavel a gente faz' }
        ]
    },
    {
        texto: 'Apaixonados pela missão',
        respostas: [
            { valor: 0, texto: 'Projetos nada mais sao do que numeros' },
            { valor: 1, texto: 'Dinheiro é bom e é isto' },
            { valor: 3, texto: 'Se meu projeto nao ajudar meu cliente entao nao fiz nada' },
            { valor: 2, texto: 'Se eu entender o que esta no codigo entao ta show' }
        ]
    },
    {
        texto: 'Ohana',
        respostas: [
            { valor: 0, texto: 'Ta com problema? Se vira filhao' },
            { valor: 3, texto: 'Voce é meu irmao de batalha, to contigo e nao solto' },
            { valor: 2, texto: 'Voce é da gti, eu tambem entao tudo certo' },
            { valor: 1, texto: 'Fora da Gti nem olho' }
        ]
    },
    {
        texto: 'Integridade',
        respostas: [
            { valor: 0, texto: 'Furar a fila do ru' },
            { valor: 2, texto: 'farol amarelo pra mim é verde' },
            { valor: 1, texto: 'Clonar cartao de credito' },
            { valor: 3, texto: 'Sou integro' }
        ]
    },
    {
        texto: 'Ser capitão da nave',
        respostas: [
            { valor: 3, texto: 'Eu serei o lider que eu quero pra mim' },
            { valor: 0, texto: 'Eu nao sabia, entao nao fiz' },
            { valor: 1, texto: 'Ve la, depois me conta' },
            { valor: 2, texto: 'To so esperando meu estagio aprovar pra sair da GTi' }
        ]
    },
    {
        texto: 'O que queremos para o Brasil é',
        respostas: [
            { valor: 1, texto: 'Mais intercâmbios para irmos embora' },
            { valor: 0, texto: 'Bater um racha' },
            { valor: 3, texto: 'Impactar com nossos projetos' },
            { valor: 2, texto: 'Progresso sem esforço' }
        ]
    }
]

//VARIAVEIS

//Guarda o valor da jogada atual
var counter = 0
//Guarda o valor da pontuacao atual
var pontuacao = 0
//Numero de perguntas existentes no objeto perguntas
var numeroDePerguntas = perguntas.length
//Itens da pagina
var itens = document.getElementsByName("resposta")

//FUNCOES

//Calcula a pontucao atual do quiz
function calcularPontuacao(questao) {
    //Se houver um item marcado para a questao, adiciona seu valor a variavel
    //pontuacao e informa sucesso retornando true
    for(var i = 0; i < 4; i++) {
        if(itens[i].checked) {
            itens[i].checked = false
            pontuacao += perguntas[questao].respostas[i].valor
            return true
        }
    }
    //Se nao há item marcado, retorna falha
    return false
}

//Preenche as 4 opcoes para questao passada
function alterarTextos(questao) {
    document.getElementById("titulo").innerHTML = perguntas[questao].texto
    for(var i = 0; i < 4; i++) {
        itens[i].nextElementSibling.innerHTML = perguntas[questao].respostas[i].texto
    }
}

//Funcao a ser chamada apos o primeiro clique no botao
function comecar() {
    //Deixa visivel as opcoes
    document.getElementById("listaRespostas").style.display = "initial"
    //Altera o texto do botao
    document.getElementById("confirmar").innerHTML = "Próxima"
    //Apaga o texto apresentado no resultado (util a partir da primeira repeticao do quiz)
    document.getElementById("resultado").innerHTML = ""
    //Apresenta as opcoes da primeira questao
    alterarTextos(0)
}

//Apresenta a proxima questao
function proxima(questao) {
    //Atualiza a pontuacao com o valor da questao anterior
    //Se o calculo retornar sucesso, apresenta a proxima questao
    if(calcularPontuacao(questao-1)) {
        alterarTextos(questao)
        return true
    }
    //Senao, retorna falha
    return false
}

//Terminar quiz
function finalizarQuiz() {
    //Atualiza a pontuacao com o valor da ultima questao
    //Se o calculo retornar sucesso, apresenta o resultado
    if(calcularPontuacao(numeroDePerguntas-1)) {
        //Esconde os itens
        document.getElementById("listaRespostas").style.display = "none"
        //Altera o titulo
        document.getElementById("titulo").innerHTML = "QUIZ DOS VALORES DA GTI"
        //Mostra o resultado em porcentagem
        document.getElementById("resultado").innerHTML = "Sua pontuação: " + parseInt(100*(pontuacao/(numeroDePerguntas*3))) +"%"
        //Altera o botao para possibilidade de recomecar o quiz
        document.getElementById("confirmar").innerHTML = "REFAZER QUIZ"
        //Inicializa as variaveis com valores padrao
        counter = -1
        pontuacao = 0
        return true
    }
    //Senao, retorna falha
    return false
}

//OBS: Em um if, quando as condicoes estao entre &&, o teste de uma destas so ocorre se a sua
//condicao a esquerda for valida

//Funcao chamada ao clicar no botao
function mostrarQuestao() {
    //Se for o primeiro click, comecamos e incrementamos o counter
    if(counter == 0) {
        comecar()
        counter++
    }
    //Se for a ultima questao, finalizamos o quiz e incrementamos o counter
    if(counter == numeroDePerguntas && finalizarQuiz()) {
        //Ao terminar a chamada de finalizarQuiz de maneira bem sucedida
        //counter tem valor -1, mas logo é incrementado para 0
        counter++
    }
    //Nas demais questoes, chamamos a proxima e incrementamos o counter
    if(counter > 0 && counter < numeroDePerguntas && proxima(counter)) {
        counter++
    }
}