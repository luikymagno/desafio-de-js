//VARIAVEIS

//A jogada comeca com o X
document.getElementById("indicadorDaVez").innerHTML = " X"
//Conta a vez da jogada atual
var jogada = 0
//Indica se houve vitória ou não
var vitoria = 0

//FUNCOES

//Realiza uma jogada
function selecionar(botao) {
    //Altera o botão se estiver vazio
    if(botao.innerHTML != "X" && botao.innerHTML != "O" && vitoria == 0) {
        var vez = document.getElementById("indicadorDaVez")
        //Se a jogada atual for ímpar, preenche com X. Caso não, com O
        if(++jogada%2 == 1) {
            botao.innerHTML = "X"
            vez.innerHTML = " O"
        }
        else {
            botao.innerHTML = "O"
            vez.innerHTML = " X"
        }
        //Verifica se, após a última jogada, houve vitória ou velha
        ganhou()
    }
}

//Zera todos as posições e recomeça o jogo
function resetar(){
    //percorre todos os botões identificados pela classe casa e os limpa
    for(i = 0; i < 9; i++) {
        document.getElementsByClassName("casa")[i].innerHTML = ""
    }
    //preenche a indicação de vencedor e de vez com o texto inicial
    document.getElementById("indicadorVencedor").innerHTML = "O vencedor é..."
    document.getElementById("indicadorDaVez").innerHTML = " X"
    //zera as variaveis
    jogada  = 0
    vitoria = 0
 }

//Verifica todos as combinações possíveis de se ganhar o jogo para X e O
function ganhou() {
    var matrix = document.getElementsByClassName("casa")
    var vencedor = document.getElementById("indicadorVencedor")
    var vez = document.getElementById("indicadorDaVez")
    //i verifica as linhas e j as colunas
    for(i = 0, j = 0; i < 9; i = i + 3, j++) {
        //Verifica se houve vitória em alguma linha
        if(matrix[i].innerHTML != "") {
            if(matrix[i].innerHTML == matrix[i + 1].innerHTML && matrix[i + 1].innerHTML == matrix[i + 2].innerHTML) {
                vez.innerHTML = ""
                vencedor.innerHTML = " O vencedor é o " + matrix[i].innerHTML
                vitoria = 1
            }
        }
        //Verifica se houve vitória em alguma coluna
        if(matrix[j].innerHTML != "") {
            if(matrix[j].innerHTML == matrix[j + 3].innerHTML && matrix[j + 3].innerHTML == matrix[j + 6].innerHTML) {
                vez.innerHTML = ""
                vencedor.innerHTML = " O vencedor é o " + matrix[j].innerHTML
                vitoria = 1
            }
        }
    }
    if(matrix[4].innerHTML != "") {
        //Verifica se houve vitória na diagonal principal
        if(matrix[0].innerHTML == matrix[4].innerHTML && matrix[4].innerHTML == matrix[8].innerHTML) {
            vez.innerHTML = ""
            vencedor.innerHTML = " O vencedor é o " + matrix[4].innerHTML
            vitoria = 1
        }
        //Verifica se houve vitória na diagonal secundária
        if(matrix[2].innerHTML == matrix[4].innerHTML && matrix[4].innerHTML == matrix[6].innerHTML) {
            vez.innerHTML = ""
            vencedor.innerHTML = " O vencedor é o " + matrix[4].innerHTML
            vitoria = 1
        }
    }
    //Se, após 9 jogadas, não houve vitória, então o jogo deu velha
    if(jogada == 9 && vitoria == 0) {
        vez.innerHTML = ""
        vencedor.innerHTML = "Deu velha"
    }
}